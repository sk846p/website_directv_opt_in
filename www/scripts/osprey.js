$(document).ready(function () {
	
	//var guidVal; //commented PROD only logic - commented as logic changed from guid to access id
	
	var localObj; //PROD only Logic
/*---------------------------------DEFAULT VIEW - Welcome Section shown & hide  yourInfo & Confirmation Section---------------------------------------------*/
	$("html, body").animate({ scrollTop: 0 }, "slow");
	$('#yourInfoSection').hide();
	$('#confirmationSection').hide();
/*--------------------------------------------WELCOME section button to show the yourInfo Section Alone ----------------------------------------------------*/
	 $('.LETS-DO-THIS').click(function() {  
		$('#welcomeSection').hide();
		$('#yourInfoSection').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		//$('#confirmationSection').hide();		 //remains hidden already
		
		// PROD ONLY LOGIC -FETCH AccessId FROM LOCAL STORAGE
		//localObj = JSON.parse(localStorage.getItem("dtvnUser"));
		//localObj = localObj["accessId"];
		//console.log(localObj);
		
		// PROD ONLY LOGIC -FETCH GUID FROM LOCAL STORAGE
		//var localObj = JSON.parse(localStorage.getItem("dtvnUser"));
		//localObj = localObj["ids"];
		//guidVal=localObj["guid"];
		 
	});  
	
//Setting REGEX for Validations
	var ZipRGEX = /^[0-9]{5}(?:-[0-9]{4})?$/;
	var NameREGEX =/^[a-z ,.'-]+$/i;
	var EmailREGEX= /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
	var AptNoREGEX= /((?:[a-zA-Z]+\s)*)([\d]+)(?:(?:\/)(\d+))?/;
	var CityREGEX=/^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$/;
	var StateREGEX=/^(A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[ANU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$/;
	var PhoneREGEX=/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;

//Setting JSON structure for API CALL
	var commObj = {}; //CommonData JSON OBJECT 
	commObj["AppName"]="SERVICES-COMMON-APP"; 


/*------------------------------------------YOURINFO section button to show the Confirmation Section Alone -------------------------------------------------*/
//Enable Button only after Filling All Fields
	$("form input[type=text]").keyup(function() {  	
		check_inputs();
	}); 

    $("form input[type=checkbox]").click(function() {   
        check_inputs();
    });
	
	function check_inputs() {
		var changecolor = false;
		$("form input[type=text]").each(function(){
			if($('.First-name').val()!= '' &&	$('.Last-name').val()!= ''  && 
				$('.Email').val()!= '' &&	$('.Street-address').val() != ''  && 
				$('.phoneNo').val() != ''  &&  $('.City').val() != ''&& 
				$('.State').val() != ''&& $('.ZIP').val() != ''	){
					validCheckbox();
			}
		});
		function validCheckbox(){	
			if($('#agree').prop('checked') === true){
			changecolor = true;
			}		
		}
        if (changecolor == true) {
			$('.Rectangle-Copy-1').css('background-color','#008cf6');
			$('.JOIN-THE-WAITLIST').css('color','#ffffff');
			console.log("color changes");
			$('#novalidinputs').hide();
			$('#alredyexits').hide();
        } 
		else {
			$('.Rectangle-Copy-1').css('background-color','#303030');
			$('.JOIN-THE-WAITLIST').css('color','#777777');
			console.log("no color change");
        }
    }
	
//FORM SUBMISSION:	
	$('.JOIN-THE-WAITLIST').click(function() {  
	
	var jsonObj = {}; //Entire JSON OBJECT 
	jsonObj["CommonData"]=commObj;
	//jsonObj["uid"]= localObj; //PROD ONLY Logic-Fetched AccessID from T-guard 
	//jsonObj["uid"]= guidVal; //PROD ONLY Logic-Fetch GUID from T-guard 
	jsonObj["uid"]= "abcd"; //HardCoded
	jsonObj["firstName"]=$('.First-name').val();
	jsonObj["lastName"]=$('.Last-name').val();
	jsonObj["emailID"]=$('.Email').val();
	jsonObj["phoneNumber"]=$('.phoneNo').val();
	jsonObj["streetAddress"]=$('.Street-address').val();
	jsonObj["aptNo"]=$('.Apt-').val();
	jsonObj["city"]=$('.City').val();
	jsonObj["state"]=$('.State').val();
	jsonObj["zip"]=$('.ZIP').val();
	console.log(jsonObj);
	
		if( $('.First-name').val().length > 0 && NameREGEX.test($('.First-name').val()) &&
			$('.Last-name').val().length > 0 && NameREGEX.test($('.Last-name').val()) &&
			$('.Email').val().length > 0 && EmailREGEX.test($('.Email').val()) &&
			$('.phoneNo').val() != ''&& PhoneREGEX.test($('.phoneNo').val()) &&
			$('.Street-address').val() != '' &&
			//$('.Apt-').val() != ''&& AptNoREGEX.test($('.Apt-').val()) && //Removed as its not a mandatory field
			$('.City').val() != ''&& NameREGEX.test($('.City').val()) &&
			$('.State').val() != ''&& NameREGEX.test($('.State').val()) &&
			($('.ZIP').val() != '' )&& ZipRGEX.test($('.ZIP').val()) &&
			$('#agree').prop('checked') === true )	{
				
			console.log("valid inputs ready for ajax call");
			$('#novalidinputs').hide();
			$('#alredyexits').hide();
			$.ajax({ 
					headers: { 
					"Content-Type":"application/json", 
					"Accept":"application/json", 
					"X-Requested-By":"ATT", 
					"Access-Control-Allow-Origin":"*" 
					}, 
					type: "POST", 
					url: "https://dev-services.stage.att.com/services-common/resources/dtv/optin", //DEV API
					//url: "https://www.att.com/apiservice/services-common/resources/dtv/optin", //PROD API
					//url: "https://www.directvnow.com/apiservice/services-common/resources/dtv/optin",
					data: JSON.stringify(jsonObj), 
					crossDomain: true, 
					success: function (result) { 
						if (result["Result"]["Code"] === 'S0000'){ 
							console.log("success -ajax posted to DB"); 
							$('#yourInfoSection').hide();
							$('#confirmationSection').show();	
							$("html, body").animate({ scrollTop: 0 }, "slow");							
						}
						else if (result["Result"]["Code"] === 'ERR0000'){ 
							console.log("Failure -ajax failed to save in DB"); 
							$('#alredyexits').show();
							
							//disable the button in this scenario
							$('.Rectangle-Copy-1').css('background-color','#303030');
							$('.JOIN-THE-WAITLIST').css('color','#777777');
								console.log("disabling button as record already exists");
						} 
						else if(result["Result"]["Code"] ==='F0003') {
							console.log("Server Error");
						}
					}, 
					error: function (e) { 
						console.log(e); //use this to see an error in ajax request 
					} 
		});
	 
		} 
		else{
			console.log("not valid inputs did not ajax");
			$('#novalidinputs').show();
	
		}
	});  

}); //End of Document.ready



